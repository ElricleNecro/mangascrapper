#!/bin/bash

for nb in $(seq 7 14)
do
	unzip ultraman_${nb}.zip
done

find tmp -type f -exec mv \{\} tmp/. \;

cd tmp
zip ../ultraman_2.zip *.jpg
cd ..
