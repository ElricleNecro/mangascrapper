#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script to extract manga scan traduction from website."""

import argparse as ap
import tempfile
import zipfile

from os import makedirs
from typing import Iterator
from requests import Session
from bs4 import BeautifulSoup

URL = "https://www.japscan.to/lecture-en-ligne/uq-holder/{tome}/{page}.html"


class MangaScantradScrapper:
    """Scrap images for manga on the japscan.to website."""
    def __init__(self, base_url: str, tome: int = 1, page: int = 1) -> None:
        """Constructor.

        :param base_url: base url for the given manga.
        :param tome: Tome number.
        :param page: Page.
        """
        # Initialising all attributes;
        self._session: Session = Session()

        self._current: BeautifulSoup = BeautifulSoup(
            self._session.get(base_url).content, "html.parser")
        self._chapter: int = 1

    @property
    def tome(self) -> int:
        return self._chapter

    def _next_chapter(self) -> bool:
        """Going to the next chapter if it exist."""
        next_url = self._current.find_all("a", {'class': "next_page"})
        if not next_url:
            return False

        html = self._session.get(next_url[0].attrs["href"])
        self._current = BeautifulSoup(html.content, "html.parser")
        self._chapter += 1

        return True

    def __iter__(self) -> Iterator[bytes]:
        """Iterator."""
        while True:
            for img in self._current.find_all(
                    "img", {'class': 'wp-manga-chapter-img'}):
                yield self._session.get(img.attrs['data-src'].strip())

            if not self._next_chapter():
                break


def zipper(files, out="/tmp/out.zip"):
    """Zip the given list of files.

    :param files: List of files to zip.
    :param out: Zip file name.
    """
    with zipfile.ZipFile(out, 'w', zipfile.ZIP_DEFLATED) as fzip:
        for fich in files:
            fzip.write(fich)


def parse_args():
    """Parse CLI arguments."""
    parser = ap.ArgumentParser()
    parser.add_argument("URL", help="URL to download.")
    parser.add_argument(
        "-o",
        "--output",
        help="Output cbz archive name.",
        type=str,
    )

    return parser.parse_args()


def main():
    """Run the main program."""
    args = parse_args()

    scrapper = MangaScantradScrapper(args.URL)
    to_zip = list()
    with tempfile.TemporaryDirectory() as tmp:
        for page, img in enumerate(scrapper):
            makedirs(f"{tmp}/Ch{scrapper.tome:03}", exist_ok=True)
            fname = f"{tmp}/Ch{scrapper.tome:03}/{page:03}.jpg"
            with open(fname, "wb") as fich:
                fich.write(img.content)
            to_zip.append(fname)

        zipper(to_zip,
               args.output if args.output is not None else f"{args.tome}.cbz")


if __name__ == "__main__":
    main()
