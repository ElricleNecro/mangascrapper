#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Script to extract manga scan traduction from website."""

import argparse as ap
import tempfile
import time
import zipfile

import cfscrape
from bs4 import BeautifulSoup


URL = "https://www.japscan.to/lecture-en-ligne/uq-holder/{tome}/{page}.html"


class JapScanScrapper:
    """Scrap images for manga on the japscan.to website."""

    def __init__(self, base_url, tome=1, page=1):
        """Constructor.

        :param base_url: base url for the given manga.
        """
        # Initialising all attributes;
        self._base_url = base_url
        self._page_end = 1
        self._current = None
        self._tome = 1
        self._page = 1

        self._scraper = cfscrape.create_scraper(delay=10)

        # If tome is set, setting it using the setter:
        self.tome = tome

        # If page is set, setting it using the setter:
        self.page = page

    def get_max_page(self):
        """Try to find out the maximum number of page for the current tome."""
        content = self.get_content().content
        try:
            select = BeautifulSoup(
                content,
                "html.parser"
            ).find("select", {'id': "pages"}).find_all("option")
        except Exception as exc:
            print(content)
            print(f"Failed on tome {self.tome}, page {self.page}")
            raise exc

        self._page_end = len(select) + 1

    def get_image(self):
        """Extract the image from the current page.

        :param inc: Should the method increment the page/tome number?
        """
        html = BeautifulSoup(self._current.content, "html.parser")
        img = html.find("div", {'id': 'image'})

        to_write = self._scraper.get(img.attrs["data-src"])

        return to_write

    def get_content(self):
        """Scrap the current URL."""
        return self._scraper.get(
            self.current_url
        )

    @property
    def tome(self):
        """Tome number."""
        return self._tome

    @tome.setter
    def tome(self, value):
        # Updating the tome number:
        self._tome = value
        # Reinitialising the page number:
        self._page = 1
        # Extracting the number of page in the chapter:
        self.get_max_page()

    @property
    def page(self):
        """Page number."""
        return self._page

    @page.setter
    def page(self, value):
        # Selecting the page while keeping it between 1 and self._page_end:
        self._page = min(
            max(1, value),
            self._page_end
        )
        # Updating the current page buffer:
        self._current = self.get_content()

    @property
    def current_url(self):
        """Format current URL."""
        return self._base_url.format(
            tome=self._tome,
            page=self._page
        )

    def __iter__(self):
        """Iterator."""
        return self

    def __next__(self):
        """Iterator."""
        if self._page >= self._page_end:
            raise StopIteration

        image = self.get_image()

        self.page += 1

        return image


def zipper(files, out="/tmp/out.zip"):
    """Zip the given list of files.

    :param files: List of files to zip.
    :param out: Zip file name.
    """
    with zipfile.ZipFile(out, 'w', zipfile.ZIP_DEFLATED) as fzip:
        for fich in files:
            fzip.write(fich)


def parse_args():
    """Parse CLI arguments."""
    parser = ap.ArgumentParser()
    parser.add_argument("URL", help="URL to download.")
    parser.add_argument(
        "-t", "--tome",
        help="Tome number.",
        type=int,
        default=1
    )
    parser.add_argument(
        "-o", "--output",
        help="Output cbz archive name.",
        type=str,
    )

    return parser.parse_args()


def main():
    """Run the main program."""
    args = parse_args()

    jap_scan = JapScanScrapper(args.URL, tome=args.tome)
    to_zip = list()
    with tempfile.TemporaryDirectory() as tmp:
        for img in jap_scan:
            fname = f"/{tmp}/{jap_scan.tome:03}_{jap_scan.page:03}.jpg"
            with open(fname, "wb") as fich:
                fich.write(img.content)
            to_zip.append(fname)

        zipper(
            to_zip,
            args.output if args.output is not None else f"{args.tome}.cbz"
        )


if __name__ == "__main__":
    main()
